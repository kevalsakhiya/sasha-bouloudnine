import requests
import pandas as pd

from lxml import html


def collect_data():
    headers = {
        'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
        'Accept': 'application/json, text/plain, */*',
        'Referer': 'https://www.lequipe.fr/',
        'sec-ch-ua-mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36'
    }

    num = 1
    hasVideo = []
    linkText = []
    titles = []
    articleId1 = []
    articleId2 = []
    articleId3 = []
    authors = []
    subtitles = []
    upVote = []
    categories = []
    texts = []
    values = []
    commentPublicationDate = []
    commentText = []
    imageURL = []
    publicationDate = []
    linkValue = []
    URL = []
    subCategory = []
    downVote = []

    while True:
        a = 0
        print(a+1)
        url = f'https://dwh.lequipe.fr/api/edito/chrono?path=/Football/&context=page&page={str(num)}'
        response = requests.request("GET", url, headers=headers)
        page_response = response.json()
        try:
            json_data = page_response.get('content').get('feed').get('items')
        except:
            json_data = None

        if json_data:
            index = 1
            for find_url in json_data:
                try:
                    json_url = find_url.get('link').get('web')
                except:
                    json_url = None
                if json_url:
                    f_url = f'https://www.lequipe.fr{json_url}'
                    page = requests.get(f_url)
                    page_con = html.fromstring(page.content)
                    category = page_con.xpath('.//*[@class="ArticleTags article__tags"]//*\
                                                [@class="ArticleTags__item has-background"]/text()')
                    if category:
                        category = (category[0]).strip()
                    else:
                        category = None
                    sub_category = page_con.xpath('.//*[@class="ArticleTags article__tags"]//*\
                                                    [@class="ArticleTags__item"]/text()')
                    if sub_category:
                        sub_category = ''.join(
                            [a.strip() for a in sub_category if a.strip()])
                    else:
                        sub_category = None
                    title = page_con.xpath('.//h1/text()')
                    if title:
                        title = (title[0]).strip()
                    else:
                        title = None
                    image_url = page_con.xpath(
                        './/*[@property="og:image"]/@content')
                    if image_url:
                        image_url = (image_url[0]).strip()
                    else:
                        image_url = None
                    subtitle = page_con.xpath('.//h2/text()')
                    if subtitle:
                        subtitle = (subtitle[0]).strip()
                    else:
                        subtitle = None
                    publication_date = page_con.xpath(
                        './/*[@class="Author__infos"]/text()')
                    if publication_date:
                        publication_date = (publication_date[0]).strip()
                    else:
                        publication_date = None
                    text = page_con.xpath(
                        './/*[@class="Paragraph__content"]//text()')
                    if text:
                        text = ''.join(text)
                    else:
                        text = None
                    has_video = page_con.xpath(
                        './/*[@class="DmVideo__player"]')
                    if has_video:
                        has_video = 'TRUE'
                    else:
                        has_video = 'FALSE'
                    text_div = page_con.xpath('.//*[@class="LinkParagraph"]')
                    for text_ in text_div:
                        link_text = text_.xpath('./a/text()')
                        if link_text:
                            link_text = (link_text[0]).strip()
                            linkText.append(link_text)
                            articleId1.append(index)
                        link_url = text_.xpath('./a/@href')
                        if link_url:
                            link_value = 'https://www.lequipe.fr' + link_url[0]
                            linkValue.append(link_value)
                    tag_div = page_con.xpath(
                        './/*[@class="Link RelatedLinks__link"]')
                    for tag in tag_div:
                        tag = tag.xpath('./text()')
                        if tag:
                            value = (tag[0]).strip()
                            values.append(value)
                            articleId2.append(index)
                    comment_url = f'https://dwh.lequipe.fr/api/efr/comments{json_url}/limits/500/lasts/0/best'
                    comment_page = requests.get(comment_url)
                    comment_response = comment_page.json()
                    json_data = comment_response.get('comments')
                    if json_data:
                        for data in json_data:
                            author = data.get('user').get('pseudo')
                            comment_publication_date = data.get('date')
                            comment_text = data.get('text')
                            num_likes = data.get('number_likes')
                            num_dislikes = data.get('number_dislikes')
                            authors.append(author)
                            commentPublicationDate.append(
                                comment_publication_date)
                            commentText.append(comment_text)
                            upVote.append(num_likes)
                            downVote.append(num_dislikes)
                            articleId3.append(index)

                            try:
                                reply_comments_data = data.get('comments')
                            except:
                                reply_comments_data = None
                            if reply_comments_data:
                                for data in reply_comments_data:
                                    author = data.get('user').get('pseudo')
                                    comment_publication_date = data.get('date')
                                    comment_text = data.get('text')
                                    num_likes = data.get('number_likes')
                                    num_dislikes = data.get('number_dislikes')
                                    authors.append(author)
                                    commentPublicationDate.append(
                                        comment_publication_date)
                                    commentText.append(comment_text)
                                    upVote.append(num_likes)
                                    downVote.append(num_dislikes)
                                    articleId3.append(index)

                    titles.append(title)
                    URL.append(f_url)
                    categories.append(category)
                    subCategory.append(sub_category)
                    imageURL.append(image_url)
                    subtitles.append(subtitle)
                    publicationDate.append(publication_date)
                    texts.append(text)
                    hasVideo.append(has_video)
                    index += 1
            num += 1
        else:
            break
        item1 = {'TITLE': titles,
                'URL': URL,
                'CATEGORY': categories,
                'SUB CATEGORY': subCategory,
                'MAIN IMAGE URL': imageURL,
                'SUBTITLE': subtitles,
                'PUBLICATION TIME': publicationDate,
                'TEXT': texts,
                'HAS VIDEO': hasVideo,
                  }

        item2 = {'ARTICLE_ID': articleId1,
                 'LINK TEXT': linkText, 
                 'LINK VALUE': linkValue, }

        item3 = {'AUTHOR': authors, 
                'PUBLICATION TIME': commentPublicationDate, 
                'TEXT': commentText,
                'UPVOTES': upVote, 
                'DOWNVOTES': downVote, 
                'ARTICLE ID': articleId3, }

        item4 = {'ARTICLE ID': articleId2, 
                'VALUE': values}
        print(item1,'\n',item2,'\n',item3,'\n',item4)
        saving_data(item1, item2, item3, item4)

def saving_data(item1, item2, item3, item4):

    df1 = pd.DataFrame(
            item1, 
            columns=['URL', 'PUBLICATION TIME', 'HAS_VIDEO', 
                        'CATEGORY', 'SUB CATEGORY', 'TITLE', 
                        'SUBTITLE', 'TEXT', 'MAIN IMAGE URL', ])
    df1['ID'] = df1.index+1
    
    updated_df1 = df1[['ID', 'URL', 'PUBLICATION_TIME', 
                    'HAS_VIDEO', 'CATEGORY', 'SUB CATEGORY',
                    'TITLE', 'SUBTITLE', 'TEXT', 'MAIN IMAGE URL', ]]
    df2 = pd.DataFrame(
                        item2, 
                        columns=['ARTICLE ID', 'LINK TEXT', 'LINK VALUE', ])
    df2['ID'] = df2.index+1
    
    updated_df2 = df2[['ID', 'ARTICLE ID', 'LINK TEXT', 'LINK VALUE', ]]
    df3 = pd.DataFrame(item3, 
                        columns=['ARTICLE ID', 'AUTHOR', 'PUBLICATION TIME', 
                                'TEXT', 'UPVOTES', 'DOWNVOTES', ])
    df3['ID'] = df3.index+1
    updated_df3 = df3[['ID', 'ARTICLE_ID', 'AUTHOR', 
                        'PUBLICATION TIME', 'TEXT', 'UPVOTES',
                        'DOWNVOTES', ]]

    df4 = pd.DataFrame(item4, 
                        columns=['ARTICLE ID', 'VALUE'])
    df4['ID'] = df4.index+1

    updated_df4 = df4[['ID', 
                        'ARTICLE ID', 
                        'VALUE']]

    writer = pd.ExcelWriter('Lequipe.xlsx', engine='xlsxwriter')
    updated_df1.to_excel(writer, sheet_name='article', index=False)
    updated_df2.to_excel(writer, sheet_name='external_link', index=False)
    updated_df3.to_excel(writer, sheet_name='comment', index=False)
    updated_df4.to_excel(writer, sheet_name='tag', index=False)
    writer.save()


if __name__ == '__main__':
    collect_data()
